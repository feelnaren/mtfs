package com.example.nlakshma.mtfs;

public class HomeViewItem {

    String name;
    String desc;
    Integer minPrice;
    String image;
    Integer view_type;
    Integer id;

    public HomeViewItem(
            String name, String imageUrl, String desc, Integer minPrice, Integer view_type, Integer id){
        this.name = name;
        this.image = imageUrl;
        this.minPrice = minPrice;
        this.desc = desc;
        this.id = id;

        if (view_type == null) {
            this.view_type = constants.DEFAULT_TYPE;
        } else {
            this.view_type = view_type;
        }

    }

}
