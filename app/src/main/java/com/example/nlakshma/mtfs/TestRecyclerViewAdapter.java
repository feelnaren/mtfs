package com.example.nlakshma.mtfs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;


public class TestRecyclerViewAdapter extends RecyclerView.Adapter<TestRecyclerViewAdapter.CustomViewHolder> {

    List<HomeViewItem> contents;
    private Context mContext;


//    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    public TestRecyclerViewAdapter(List<HomeViewItem> contents, Context mContext) {
        this.contents = contents;
        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_CELL;
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_big, parent, false);
        return new CustomViewHolder(    view) {};
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        HomeViewItem feedItem = contents.get(position);
        Picasso.with(mContext).load(feedItem.image).into(holder.imageView);
        holder.textView.setText(feedItem.name);
        holder.descTextView.setText(feedItem.desc);
        holder.minPriceTextView.setText(
                feedItem.minPrice.toString() + " " + mContext.getResources().getString(R.string.Rs));
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;
        protected TextView descTextView;
        protected TextView minPriceTextView;

        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            this.textView = (TextView) view.findViewById(R.id.textView);
            this.descTextView = (TextView) view.findViewById(R.id.textViewDesc);
            this.minPriceTextView = (TextView) view.findViewById(R.id.textViewPrice);
        }
    }
}