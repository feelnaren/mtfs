package com.example.nlakshma.mtfs;

import android.content.Context;
import android.content.Intent;
import android.media.Rating;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryCVAdapter extends RecyclerView.Adapter<CategoryCVAdapter.CustomViewHolder> {
    List<HomeViewItem> contents;
    private Context mContext;

    public CategoryCVAdapter(ArrayList dataArgs, Context context){
        contents = dataArgs;
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == contents.size()) {
            return constants.CALLBACK_TYPE;
        } else {
            return constants.DEFAULT_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if (viewType == constants.CALLBACK_TYPE) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.callback_request, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_card_small, parent, false);
        }

        return new CustomViewHolder(view) {};
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        HomeViewItem feedItem = contents.get(position);

        if (feedItem.view_type == constants.CALLBACK_TYPE) {
            // Nothing but
            // Register for callback handle
            holder.phone.setText(Helpr.getUsername(mContext));
        } else {
            Picasso.with(mContext).load(feedItem.image).into(holder.imageView);
            holder.textView.setText(feedItem.name);
            holder.minPriceTextView.setText(
                    feedItem.minPrice.toString() + " " + mContext.getResources().getString(R.string.Rs));
            holder.rating.setRating(Helpr.genRandomRating());
        }
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected ImageView imageView;
        protected TextView textView;
        protected TextView minPriceTextView;
        protected TextView phone;
        protected RatingBar rating;

        public CustomViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.imageView);
            this.textView = (TextView) view.findViewById(R.id.textView);
            this.minPriceTextView = (TextView) view.findViewById(R.id.textViewPrice);
            this.phone = (TextView) view.findViewById(R.id.phone);
            this.rating = (RatingBar) view.findViewById(R.id.categoryRating);

        }


    }

}