package com.example.nlakshma.mtfs;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nlakshma.mtfs.interfaces.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class Login extends BaseActivity implements AsyncResponse {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_login);
        //taskforsure.com/auth/api-token-auth
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        HttpCallPost httpcall = new HttpCallPost();
//        Toast.makeText(
//                getApplicationContext(),
//                intent.getStringExtra("OTP"),
//                Toast.LENGTH_SHORT).show();
        String otp = intent.getStringExtra("OTP");

        EditText phoneField = (EditText) findViewById(R.id.name);
        String phone = phoneField.getText().toString();

        HashMap data = new HashMap();
        data.put("url", constants.host + "/auth/verify_otp");

        HashMap params = new HashMap();
        params.put("otp", otp);
        params.put("mobile", phone);
        data.put("params", params);
        data.put("caller", "verify_otp");

        httpcall.execute(data);
        httpcall.delegate = this;
    }

    public void logMeIn(View view) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        HttpCallPost httpcall = new HttpCallPost();
        EditText phoneField = (EditText) findViewById(R.id.name);
        String phone = phoneField.getText().toString();

        if (phone.length() < 10) {
            Toast.makeText(
                    getApplicationContext(),
                    "Please enter proper phone number",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        HashMap data = new HashMap();
        data.put("url", constants.host + "/auth/send_otp");

        HashMap params = new HashMap();
        params.put("mobile", phone);
        data.put("params", params);
        data.put("caller", "send_otp");

        httpcall.execute(data);
        httpcall.delegate = this;
    }

    public void processFinish(String data, String caller) {
        try {
            Toast.makeText(getApplicationContext(), data.toString(), Toast.LENGTH_LONG).show();
            JSONObject jd = new JSONObject(data);

            if (caller == "send_otp") {
                sentOTP(jd);
            }

            if (caller == "verify_otp") {
                verifyOTP(jd);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void sentOTP(JSONObject data) {
        // Do nothing as we need to wait for SMS
        Toast t = Toast.makeText(getApplicationContext(), "IN send otp LOGIN", Toast.LENGTH_SHORT);
        t.show();
    }

    public void verifyOTP(JSONObject data) {
        // ALSO UPDATE NAME EMAIL
        try {
            Helpr.setToken(this, data.get("_").toString());
            Helpr.setUsername(this, data.get("name").toString());
            Toast.makeText(getApplicationContext(), "In verify otp LOGIN", Toast.LENGTH_SHORT).show();
            launchHome();
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void launchHome() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }

}
