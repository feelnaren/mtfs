package com.example.nlakshma.mtfs;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.example.nlakshma.mtfs.interfaces.AsyncResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class WelcomeScreen extends AppCompatActivity implements AsyncResponse {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            TypefaceProvider.registerDefaultIconSets();

            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.activity_welcome_screen);
            navigate();
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        public void navigate() {
            Toast t = Toast.makeText(getApplicationContext(), "In welcome screen navigate", Toast.LENGTH_SHORT);
            t.show();

            if (com.example.nlakshma.mtfs.Helpr.getToken(this) == null){
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        launchLogin();
                    }
                }, 2000);

            } else {
                // Launch home screen if token is not null
                verifyAuthToken();
            }

        }


        public void launchLogin(){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }


        public void launchHome() {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
        }


        public void processFinish(String data, String caller) {

            if (caller == constants.CALLER_VERIFY_TOKEN) {
                try {
                    JSONObject jsonData = new JSONObject(data);
                    Boolean status = (Boolean) jsonData.get("status");
                    if (status == true) {
                        launchHome();
                        return;
                    }
                } catch (JSONException e) {
                    Log.e("welcome screen", e.toString());
                }
            }

            // Default go to login
            launchLogin();
        }


        public void verifyAuthToken() {
            String phone = Helpr.getUsername(getApplicationContext());
            String token = Helpr.getToken(getApplicationContext());
            HashMap data = new HashMap();

            data.put("url", constants.host + "/auth/verify-token-auth");
            HttpCallPost httpcall = new HttpCallPost();
            HashMap params = new HashMap();
            params.put("username", phone);
            params.put("token", token);
            data.put("params", params);
            data.put("caller", constants.CALLER_VERIFY_TOKEN);

            httpcall.execute(data);
            httpcall.delegate = this;
        }


    }
