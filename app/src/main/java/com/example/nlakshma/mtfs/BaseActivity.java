package com.example.nlakshma.mtfs;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.nlakshma.mtfs.R;

/**
 * Created by nlakshma on 1/5/16.
 */
public class BaseActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Toolbar mToolbar;
    public ProgressDialog progressDialog;
    private FragmentDrawer drawerFragment;

    protected void onCreate(Bundle savedInstanceState, int resLayoutID) {
        super.onCreate(savedInstanceState);
        setContentView(resLayoutID);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void showLoading(String title, String message){
        if (progressDialog == null) {
            // in standard case YourActivity.this
            progressDialog = ProgressDialog.show(this, title, message, true);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }
    }

    public void hideLoading(){
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        switch (position) {
            case 0:
                Intent intent = new Intent(this, Home.class);
                startActivity(intent);
                break;
            case 1:
                Intent loginIntent = new Intent(this, Login.class);
                startActivity(loginIntent);
                break;
            case 2:
                Intent profileIntent = new Intent(this, com.example.nlakshma.mtfs.profile.class);
                startActivity(profileIntent);
                break;
            case 3:
                Intent addressIntent = new Intent(this, com.example.nlakshma.mtfs.Addresses.class);
                startActivity(addressIntent);
                break;
            default:
                break;
        }
    }

}

