package com.example.nlakshma.mtfs;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.nlakshma.mtfs.constants;

/**
 * Created by nlakshma on 10/23/15.
 */
public class Helpr {

    public static void setToken(Context context, String value){
        setValue(context, constants.TOKEN_KEY, value);
    }

    public static void setUsername(Context context, String value){
        setValue(context, constants.USERNAME, value);
    }

    public static void setValue(Context context, String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(constants.PREF_FILE, constants.PREF_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getToken(Context context) {
        return getValue(context, constants.TOKEN_KEY);
    }

    public static String getUsername(Context context) {
        return getValue(context, constants.USERNAME);
    }

    public static String getValue(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(constants.PREF_FILE, constants.PREF_PRIVATE);
        return prefs.getString(key, null);
    }

    public static Integer genRandomRating() {
        return 3 + (int)(Math.random() * ((5 - 3) + 1));
    }

}
