package com.example.nlakshma.mtfs;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nlakshma.mtfs.interfaces.AsyncResponse;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class options extends BaseActivity implements AsyncResponse {

    JSONObject jd;
    HashMap jsonData;
    private Calendar calendar;
    private EditText dateInput;
    private int year, month, day;
    DatePickerDialog.OnDateSetListener date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_options);
        init();

    }

    public void init() {
        Intent intent = getIntent();
        jsonData = (HashMap)intent.getExtras().get(constants.CATEGORY_OPTIONS_INTENT);
        Toast.makeText(getApplicationContext(), "DATA :" + jsonData.toString(), Toast.LENGTH_SHORT).show();
        getData(Integer.parseInt(jsonData.get("id").toString()));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getData(Integer id) {
        showLoading("Loading..", "Collecting info");
        String url = String.format(constants.host + "/api/v1/service_category?id=%d", id);
        HttpCall httpcall = new HttpCall();
        httpcall.execute(url);
        httpcall.delegate = this;
    }

    public void setBanner(JSONObject catJ){
        try {
            ImageView img = (ImageView) findViewById(R.id.optionsImageView);
            Picasso.with(this).setIndicatorsEnabled(true);
            Picasso.with(this)
                    .load(catJ.get("pic_url").toString())
                    .fit().centerCrop()
                    .into(img);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.w("set BAnner", e.toString());
        }
    }

    public void processFinish(String data, String caller) {
        try {
            jd = new JSONObject(data);
            JSONObject catJsonData = (JSONObject)jd.get("category");
            setBanner(catJsonData);
            setDescriptions(catJsonData);
//            setOptions(jd, catJsonData);
            regSetDate();
            setOptions();

//            String title_str = catJsonData.get("name").toString();
//            TextView title = (TextView) findViewById(R.id.title);
//            title.setText(title_str);
//            setTitle(title_str);
            hideLoading();
        } catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setDescriptions(JSONObject catJ){
        try {
            TextView htmlTextView = (TextView)findViewById(R.id.extraHTML);
            htmlTextView.setText(Html.fromHtml(catJ.get("extra_html").toString(), null, null));
        } catch (JSONException e) {
            e.printStackTrace();
            Log.w("set BAnner", e.toString());
        }
    }

    public String formatDate(Integer year, Integer month, Integer day) {
        return day + "/" + month + "/" + year;
    }


    private void regSetDate() {
        dateInput = (EditText) findViewById(R.id.order_date);
        Calendar mcurrentDate=Calendar.getInstance();

        year=mcurrentDate.get(Calendar.YEAR);
        month=mcurrentDate.get(Calendar.MONTH);
        day=mcurrentDate.get(Calendar.DAY_OF_MONTH);
        dateInput.setText(formatDate(year, month + 1, day));

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                dateInput.setText(formatDate(selectedyear, selectedmonth + 1, selectedday));
            }

        };

        dateInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog mDatePicker = new DatePickerDialog(options.this, date, year, month, day);
                mDatePicker.setTitle("Select service date");
                mDatePicker.show();
            }
        });
    }


    public void setOptions(){
        try {
            JSONObject catJsonData = (JSONObject) jd.get("category");
            JSONArray serviceOptions = (JSONArray) jd.get("disctinct_options");
            JSONArray timeSlotsOptions = (JSONArray) catJsonData.get("time_slots");
            JSONObject fieldNames = (JSONObject) jd.get("field_names");
            Toast.makeText(getApplicationContext(), "DATA :" + serviceOptions.toString(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Log.e("mtfs", "Error");
        }
    }


    public void setSpinner(Integer spinnerId, ArrayList options) {
        Spinner spinner = (Spinner) findViewById(spinnerId);

        // Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, options);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);
    }


}
