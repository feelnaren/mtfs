package com.example.nlakshma.mtfs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.example.nlakshma.mtfs.fragment.RecyclerViewFragment;
import com.example.nlakshma.mtfs.interfaces.AsyncResponse;
import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Home extends BaseActivity implements AsyncResponse{

    private Toolbar mToolbar;
    private MaterialViewPager mViewPager;
    private JSONObject jsonData;
    ArrayList listDataHeader;
    HashMap listDataChild;
    HashMap <String, String> carousel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_home);
        init();
    }


    public void init(){
        showLoading("Loading data", "Fetching services");
        getData();
    }

    public void RenderViewPager (){
        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);
        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                String heading = listDataHeader.get(position).toString();
                ArrayList <HashMap> items = (ArrayList) listDataChild.get(heading);
                return RecyclerViewFragment.newInstance(items, Home.this);
            }

            @Override
            public int getCount() {
                return listDataHeader.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return listDataHeader.get(position).toString();
            }
        });

        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                Integer carouselLength = listDataHeader.size();
                Integer index = page % carouselLength;

                String heading = listDataHeader.get(page).toString();
                String imageUrl = carousel.get(heading);

                ArrayList <Integer> colors = new ArrayList();
                colors.add(R.color.green);
                colors.add(R.color.blue);
                colors.add(R.color.red);
                colors.add(R.color.green_teal);
                return HeaderDesign.fromColorResAndUrl(colors.get(index), imageUrl);
            };
        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        hideLoading();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void processFinish(String data, String caller) {
        try {
            Log.w("app", data.toString());
            jsonData = new JSONObject(data);
            prepareListData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RenderViewPager();
    }

    public void getData(){
        HttpCall httpcall = new HttpCall();
        httpcall.execute(constants.host + "/api/v1/service_type_groups");
        httpcall.delegate = this;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<HashMap>>();
        carousel = new HashMap<String, String>();

        try {
            JSONArray groups = (JSONArray) jsonData.get("groups");
            for(int j = 0; j < groups.length(); j++) {
                JSONObject st = groups.getJSONObject(j);
                String groupKey = (String) st.get("name");

                // update carousel
                String carouselUrl = (String) st.get("app_pic_url");
                carousel.put(groupKey, carouselUrl);

                //update service types
                JSONArray groupDataList = (JSONArray) st.get("service_types");
                listDataHeader.add(groupKey);
                List<HashMap> items = new ArrayList<HashMap>();

                for (int i = 0; i < groupDataList.length(); i++) {
                    JSONObject jsn = groupDataList.getJSONObject(i);
                    HashMap hm = new HashMap();
                    hm.put("name", jsn.get("name"));
                    hm.put("image_url", jsn.get("pic_rect"));

                    // Don't care if min price not available
                    try {
                        hm.put("min_price", jsn.get("min_price"));
                    } catch (JSONException e) {
                        hm.put("min_price", 0);
                    }

                    hm.put("desc", jsn.get("desc"));
                    JSONArray cats = (JSONArray)jsn.get("service_categories");
                    hm.put("categories", cats);
                    items.add(hm);
                }
                    listDataChild.put(groupKey, items);
            }
        } catch (JSONException e) {
            Log.e("Error", e.toString());
            e.printStackTrace();
        }
    }



}
