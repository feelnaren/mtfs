package com.example.nlakshma.mtfs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class IncominSms extends BroadcastReceiver {

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();

    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    if (senderNum.endsWith("TASKFS") == true) {
                        String otp = parseOTP(message);
                        if (otp != "") {
                            Intent callingIntent = new Intent(context, Login.class);
                            callingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callingIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            callingIntent.putExtra("OTP", otp);
                            context.startActivity(callingIntent);
                        }
                    }
                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);

        }
    }

    public static String parseOTP(String mess){
        if (mess.contains("OTP") == true){
            String[] messBits = mess.split(" ");
            return messBits[3];
        }
        return "";
    }
}