package com.example.nlakshma.mtfs.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.nlakshma.mtfs.Categories;
import com.example.nlakshma.mtfs.Home;
import com.example.nlakshma.mtfs.HomeViewItem;
import com.example.nlakshma.mtfs.RecyclerItemClickListener;
import com.example.nlakshma.mtfs.constants;
import com.github.florent37.materialviewpager.MaterialViewPagerHelper;
import com.github.florent37.materialviewpager.adapter.RecyclerViewMaterialAdapter;
import com.example.nlakshma.mtfs.R;
import com.example.nlakshma.mtfs.TestRecyclerViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecyclerViewFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private Context mContext;
    private List<HomeViewItem> mContentItems = new ArrayList<>();
    public ArrayList items;

    public static RecyclerViewFragment newInstance(ArrayList items, Context context) {
        RecyclerViewFragment temp = new RecyclerViewFragment();
        temp.setItems(items, context);
        return temp;
    }

    public void setItems(ArrayList items, Context context){
        this.items =  items;
        this.mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(
            new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Integer index = position - 1;
                    Toast.makeText(mContext, "Clicked " + position, Toast.LENGTH_SHORT).show();
                    Log.w("test", items.get(index).toString());
                    Intent intent = new Intent(mContext, Categories.class);
                    HashMap item = (HashMap) items.get(index);
                    HashMap result = convertData(item);
                    intent.putExtra(constants.CATEGORIES_INTENT, result);
                    startActivity(intent);
                }

                public HashMap convertData(HashMap partialJson) {
                    HashMap results = new HashMap();
                    try {
                        results.put("name", partialJson.get("name"));
                        results.put("image_url", URLEncoder.encode(partialJson.get("image_url").toString()));
                        results.put("min_price", partialJson.get("min_price"));
                        JSONArray cats = (JSONArray) partialJson.get("categories");
                        ArrayList categories = new ArrayList();

                        for (int i = 0; i < cats.length(); i++) {
                            JSONObject cat = (JSONObject) cats.get(i);
                            HashMap category = new HashMap();
                            category.put("name", cat.get("name"));
                            category.put("image_url", cat.get("pic_url").toString());
                            category.put("min_price", cat.get("min_price"));
                            category.put("id", cat.get("id"));
                            categories.add(category);
                        }
                        results.put("categories", categories);
                    } catch(JSONException e){
                        e.printStackTrace();
                    }
                    return results;
                }
            }));

            mAdapter=new
            RecyclerViewMaterialAdapter(new TestRecyclerViewAdapter(mContentItems, mContext));
            mRecyclerView.setAdapter(mAdapter);

            if(this.items!=null)

            {
                for (int i = 0; i < this.items.size(); ++i) {
                    HashMap item = (HashMap) items.get(i);
                    mContentItems.add(new HomeViewItem(
                            item.get("name").toString(),
                            item.get("image_url").toString(),
                            item.get("desc").toString(),
                            Integer.parseInt(item.get("min_price").toString()),
                            null,
                            0
                    ));
                    mAdapter.notifyDataSetChanged();
                }
            }

            MaterialViewPagerHelper.registerRecyclerView(
                    getActivity(), mRecyclerView,
                    null);
        }
    }