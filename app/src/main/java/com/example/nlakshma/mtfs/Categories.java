package com.example.nlakshma.mtfs;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;


public class Categories extends BaseActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private HashMap results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_categories);


        Intent intent = getIntent();
        results = (HashMap) intent.getExtras().get(constants.CATEGORIES_INTENT);

        recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        final HashMap categoryItems = prepareCategoryItem();
        adapter = new CategoryCVAdapter((ArrayList) categoryItems.get("categories"), this);
        setTitle(categoryItems.get("name").toString());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ArrayList categories = (ArrayList) categoryItems.get("categories");
                        HomeViewItem category = (HomeViewItem) categories.get(position);
                        Toast.makeText(getApplicationContext(), "Clicked a category:" + category.id.toString(), Toast.LENGTH_SHORT).show();
                        launchCategoryOptions(category.id);
                    }

                    public void launchCategoryOptions(Integer id) {
                        Intent intent = new Intent(getApplicationContext(), options.class);
                        HashMap data = new HashMap();
                        data.put("id", id);
                        intent.putExtra(constants.CATEGORY_OPTIONS_INTENT, data);
                        startActivity(intent);
                    }

                })
        );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_categories, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public HashMap prepareCategoryItem(){
            HashMap CategoryItems = new HashMap();
            CategoryItems.put("name", results.get("name"));
            CategoryItems.put("image_url", results.get("image_url").toString());
            CategoryItems.put("min_price", results.get("min_price"));
            ArrayList cats = (ArrayList) results.get("categories");
            ArrayList categories = new ArrayList();

            for (int i = 0; i < cats.size(); i++) {
                HashMap cat = (HashMap) cats.get(i);
                HomeViewItem category = new HomeViewItem(
                        cat.get("name").toString(),
                        cat.get("image_url").toString(),
                        "",
                        Integer.parseInt(cat.get("min_price").toString()),
                        null,
                        Integer.parseInt(cat.get("id").toString())
                );
                categories.add(category);
            }

            // Add a callback request card
            HomeViewItem callback = new HomeViewItem(
                    "Callback", "callback", "callback", 0, constants.CALLBACK_TYPE, null);
            categories.add(callback);

            CategoryItems.put("categories", categories);
            return CategoryItems;
    }

}
