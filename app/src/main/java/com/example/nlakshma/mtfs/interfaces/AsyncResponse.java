package com.example.nlakshma.mtfs.interfaces;

/**
 * Created by nlakshma on 10/2/15.
 */
public interface AsyncResponse {
    void processFinish(String output, String caller);
}
