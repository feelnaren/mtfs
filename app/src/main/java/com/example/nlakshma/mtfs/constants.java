package com.example.nlakshma.mtfs;

/**
 * Created by nlakshma on 10/23/15.
 */
public class constants {
    public  static String USERNAME = "tfs_username";
    public static Integer PREF_PRIVATE = 0;
    public static String host = "http://staging.taskforsure.com";
    public static String CALLER_VERIFY_TOKEN = "verify_token";
    public static String TOKEN_KEY = "tfs_token_key";
    public static String PREF_FILE = "tfs_pref_file";
    public static String CATEGORIES_INTENT = "categories_intent";
    public static String  CATEGORY_OPTIONS_INTENT = "category_options_intent";

    public static Integer DEFAULT_TYPE = 0;
    public static Integer CALLBACK_TYPE = 1;
}
