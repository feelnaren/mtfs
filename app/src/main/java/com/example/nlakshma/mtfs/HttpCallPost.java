package com.example.nlakshma.mtfs;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.nlakshma.mtfs.interfaces.AsyncResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.message.BasicNameValuePair;

/**
 * Created by nlakshma on 10/2/15.
 */

public class HttpCallPost extends AsyncTask<HashMap, Void, String> {

    public AsyncResponse delegate=null;
    public Boolean useHeader = false;
    public String accessToken;
    public HashMap <String, String> params;
    public String caller = "";
    private final String USER_AGENT = "Mozilla/5.0";


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(HashMap... postData) {
        try {
            HashMap pdata = postData[0];

            String url = pdata.get("url").toString();

            if (pdata.containsKey("use_header")) {
                useHeader = (Boolean) pdata.get("use_header");
            }

            if (pdata.containsKey("access_token")) {
                accessToken = (String) pdata.get("access_token");
            }

            if (pdata.containsKey("caller")) {
                caller = pdata.get("caller").toString();
            }

            params = (HashMap) pdata.get("params");
            ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
            for (Map.Entry<String, String> entry : params.entrySet()) {
                parameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            }

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("User-Agent", USER_AGENT);

            try {
                httpPost.setEntity(new UrlEncodedFormEntity(parameters));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (useHeader == true) {
                httpPost.setHeader("Authorization", "Token " + accessToken);
            }

            HttpResponse response = httpClient.execute(httpPost);
            Log.d("Http Post Response:", response.toString());

            int status = response.getStatusLine().getStatusCode();

            if (status == 200) {
                HttpEntity respEntity = response.getEntity();
                String data = EntityUtils.toString(respEntity);
                return data.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    protected void onPostExecute(String result) {
        delegate.processFinish(result, caller);
    }

}
