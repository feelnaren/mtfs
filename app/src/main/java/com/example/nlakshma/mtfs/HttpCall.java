package com.example.nlakshma.mtfs;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.nlakshma.mtfs.interfaces.AsyncResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by nlakshma on 10/2/15.
 */

public class HttpCall extends AsyncTask<String, Void, String> {

    public AsyncResponse delegate=null;
    private final String USER_AGENT = "Mozilla/5.0";

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... urls) {
        try {

            HttpGet httpget = new HttpGet(urls[0]);
            httpget.setHeader("User-Agent", USER_AGENT);
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httpget);

            int status = response.getStatusLine().getStatusCode();

            if (status == 200) {
                HttpEntity entity = response.getEntity();
                String data = EntityUtils.toString(entity);
                return data.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    protected void onPostExecute(String result) {
        delegate.processFinish(result, "");
    }

}
